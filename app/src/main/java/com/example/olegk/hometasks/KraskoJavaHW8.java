package com.example.olegk.hometasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Scanner;

/**
 * Created by Oleg K on 03.04.2017.
 */


public class KraskoJavaHW8 {
    public static void fromInToFile(){

        Scanner scanner = new Scanner(System.in);
        System.out.println("enter name of file");
        String name = scanner.nextLine();
        File file = new File(name + ".org");
        try {
            if (file.exists()){
                System.out.println("File with the same name exists, if you want to create new file - enter 1 and write new name for file, if you want rewrite the file enter 0");
                int ifExists = Integer.parseInt(scanner.next());
                if (ifExists == 1){
                    File file1 = new File(scanner.next() + ".org");
                    file = file1;
                }
            }
        }
        catch (NumberFormatException ex){
            System.out.println(ex.getMessage());
        }

        int flag = 1;
        int i = 0;
        double salary = 0;
        String number = "0";
        boolean isNumber = false;
        boolean isDouble = false;
        try (Scanner scan = new Scanner(System.in);
             FileWriter fw = new FileWriter(file);){
            while (flag == 1) {
                System.out.println("enter Last Name");
                fw.write(scan.next());
                fw.write(", ");
                System.out.println("enter First Name");
                fw.write(scan.next());
                fw.write(", ");
                System.out.println("enter position");
                fw.write(scan.next());
                fw.write(", ");
                System.out.println("enter gender(Male/Female)");
                fw.write(scan.next());
                fw.write(", ");
                while (number.length() != 4 || isNumber == false){
                    isNumber = false;
                    System.out.println("enter number(4 digits)");
                    number = scan.next();
                    try {
                        i = Integer.parseInt(number);
                        isNumber = true;
                    }
                    catch (NumberFormatException ex){
                        System.out.println("not a number");
                    }
                }
                //number = Integer.toString(i);
                fw.write(number);
                fw.write(", ");
                System.out.println("enter salary");
                while (isDouble == false)
                    try {
                        salary = Double.parseDouble(scan.next());
                        isDouble = true;
                        BigDecimal bigDecimal = new BigDecimal(salary);
                        number = bigDecimal.setScale(2, BigDecimal.ROUND_CEILING).toString();
                        fw.write(number);
                    }
                    catch (NumberFormatException ex){
                        System.out.println("not a number");
                    }
                fw.write("\n");
                System.out.println("more? if yes - enter 1, not - enter 0");
                try{
                    flag = Integer.parseInt(scan.next());
                }
                catch (NumberFormatException ex){
                    System.out.println(ex.getMessage());
                }
                number = "0";
                isDouble = false;
                isNumber = false;
            }
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {

        fromInToFile();
    }

}
