package com.example.olegk.hometasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Created by Oleg K on 27.03.2017.
 */

public class Lesson26_03 {
    public static void main(String[] args) throws IOException, IOException {

        //getting name and mark
        Scanner scan = new Scanner(System.in);
        System.out.println("enter name");
        String name = scan.nextLine();
        System.out.println("and mark");
        double mark = scan.nextDouble();
        //scan.close();


        String fileS = "averageMark";
        GregorianCalendar cal = new GregorianCalendar();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        File file = new File(fileS + day + month + ".txt");
        scan = new Scanner(System.in);
        if(file.exists()){
            System.out.println("file is exist, please rename the file");
            File f = new File(scan.nextLine());
            file = f;
        }


        //inserting new record
        FileWriter fw = new FileWriter(file);
        Scanner scan1 = new Scanner(new FileReader("task1.txt"));
        boolean flag = true;
        while(scan1.hasNextLine()){
            String input = scan1.nextLine();
            String [] s = input.split(" ");
            double currentMark = Double.parseDouble(s[1]);
            if((mark > currentMark) && (flag == true)){
                fw.write(name + " " + mark + "\n");
                fw.write(input + "\n");
                flag = false;
            }
            else{
                fw.write(input + "\n");
            }
        }
        if(flag == true){
            fw.write(name + " " + mark + "\n");
        }
        fw.close();
        scan1.close();
    }
}
