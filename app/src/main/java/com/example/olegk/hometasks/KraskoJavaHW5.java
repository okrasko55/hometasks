package com.example.olegk.hometasks;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Oleg K on 14.03.2017.
 */



public class KraskoJavaHW5 {

    public static void generateArrDoubleRange(double arr[], int min, int max){
        Random random = new Random();
        for (int i = 0, N = arr.length; i < N; i++){
            arr[i] = min + random.nextInt(max - min + 1);
            arr[i] += Math.random();
        }
    }
    public static void displayArrDouble(double [] arr){
        for(int i = 0, N = arr.length; i < N; i++){
            System.out.println(i + " : " + arr[i]);
        }
    }

    public static int partition (double [] array, int start, int end) {
        int marker = start;
        for ( int i = start; i <= end; i++ ) {
            if ( array[i] <= array[end] ) {
                double temp = array[marker]; // swap
                array[marker] = array[i];
                array[i] = temp;
                marker += 1;
            }
        }
        return marker - 1;
    }
    public static void quicksort (double [] array, int start, int end) {
        if ( start >= end ) {
            return;
        }
        int pivot = partition (array, start, end);
        quicksort (array, start, pivot-1);
        quicksort (array, pivot+1, end);
    }



    public static void main(String[] args) {

        //first part of task
        double [] arr = new double [10];
        generateArrDoubleRange(arr, 1, 10);
        displayArrDouble(arr);
        quicksort(arr, 0, 9);
        displayArrDouble(arr);


        //second part of task
        int min = -1;
        int max = 0;
        while (min < 1) {
            try {
                System.out.println("please enter the minimum point of range");
                Scanner scann = new Scanner(System.in);
                min = Integer.parseInt(scann.nextLine());
                if (min < 1)
                    throw new Exception();
            }
            catch (NumberFormatException ex) {
                System.out.println("entered value should be integer and nothing else");
            }
            catch (Exception e) {
                System.out.println("number should be more or equal 1 ");
            }
        }
        while (max <= min) {
            try {
                System.out.println("please enter the maximum point of range");
                Scanner scann = new Scanner(System.in);
                max = Integer.parseInt(scann.nextLine());
                if ( max <= min)
                    throw new Exception();
            }
            catch (NumberFormatException ex) {
                System.out.println("entered value should be integer and nothing else");
            }
            catch (Exception e) {
                System.out.println("number should be more than minimum");
            }
        }

        double [] arr2 = new double [100000];
        generateArrDoubleRange(arr2, min, max);
        quicksort(arr2, 0, 99999);
    }
}
