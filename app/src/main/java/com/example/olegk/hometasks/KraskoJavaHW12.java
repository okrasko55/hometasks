package com.example.olegk.hometasks;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;



/**
 * Created by Oleg K on 21.04.2017.
 */

public class KraskoJavaHW12 {
    /**
     *
     * @param from
     * @param to
     * @param text
     * @param scanner
     * @return
     */
    public static int parseIntFromIn(int from, int to, String text, Scanner scanner){
        int i = 0;
        while (i < from || i > to) {
            try {
                System.out.println(text);
                i = Integer.parseInt(scanner.nextLine());
                if (i < from || i > to){
                    throw new Exception();
                }
            } catch (NumberFormatException ex) {
                System.out.println("not a number");
            } catch (Exception e){
                System.out.println("number is in the wrong range");
            }
        }
        return i;
    }

    /**
     *
     * @param from
     * @param to
     * @param text
     * @param scanner
     * @return
     */
    public static double parseDoubleFromIn(int from, int to, String text, Scanner scanner) {
        double d = 0;
        while (d < from || d > to) {
            try {
                System.out.println(text);
                d = Double.parseDouble(scanner.nextLine());
                if (d < from || d > to){
                    throw new Exception();
                }
            } catch (NumberFormatException ex) {
                System.out.println("not a number");
            } catch (Exception e){
                System.out.println("number is in the wrong range");
            }
        }
        return d;
    }

    /**
     *
     * @param capacity
     * @param text
     * @param scanner
     * @return
     */
    public static double[] getDoubleArrayFromIn(int from, int to, int capacity, String text, Scanner scanner){
        double [] array = new double[capacity];
        int i = 0;
        while (i < capacity){
            while (array[i] < 1 || array[i] > 5) {
                array[i] = parseDoubleFromIn(from, to, text, scanner);
            }
            i++;
        }
        return array;
    }
    public static String getNameFromIn(String text, Scanner scanner){
        System.out.println(text);
        return scanner.nextLine();
    }


    public static GregorianCalendar getDate(Scanner scanner){
        int day = 0;
        GregorianCalendar dateOfBirth = new GregorianCalendar();
        int year = parseIntFromIn(1900, 2017, "enter year of birth", scanner);
        int month = parseIntFromIn(1, 12, "enter month of birth", scanner);
        if (month == 2 && dateOfBirth.isLeapYear(year)){
            day = parseIntFromIn(1, 29, "enter day of birth", scanner);
        }
        else if (month == 2 && !dateOfBirth.isLeapYear(year)){
            day = parseIntFromIn(1, 28, "enter day of birth", scanner);
        }
        else if (month == 4 || month == 6 || month == 9 || month == 11){
            day = parseIntFromIn(1, 30, "enter day of birth", scanner);
        }
        else {
            day = parseIntFromIn(1, 31, "enter day of birth", scanner);
        }
        dateOfBirth.set(year, month - 1, day);
        return dateOfBirth;
    }

    public static void calendarOfBirthdays(List<Student> list){
        Collections.sort(list, Student.getSortByBirthday());
        for (Student s:
             list) {
            System.out.println("\"" + s.getName() + "\"" + " - " + s.getDate().getTime());
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Student> list = new ArrayList<>();
        String [] texts = {"enter name please", "enter mark please (should be more or equals 1.0 and less or equals 5.0)"};
        int flagOfQty = 0;
        while (flagOfQty == 0){
            list.add(new Student(getNameFromIn(texts[0], scanner), getDoubleArrayFromIn(1, 5, 5, texts[1], scanner), getDate(scanner)));
            System.out.println("enough? if yes - 1, no - 0 and other");
            try{
                flagOfQty = Integer.parseInt(scanner.nextLine());
            }
            catch (NumberFormatException ex){
                System.out.println(ex.getMessage());
            }
        }
        Collections.sort(list, Student.getSort());


        for (Student s:
             list) {
            System.out.println(s.toString());
        }

        calendarOfBirthdays(list);

    }
}


class Student /*implements Comparable<Student>*/{

    //variables
    private String name;
    private double [] marks;
    private double averageMark;
    private GregorianCalendar date;

    //methods
    public double gerAverageMark(){
        double sum = 0;
        for (double d:
             marks) {
            sum += d;
        }
        return sum / 5.0;
    }

    //constructors

    /**
     *
     * @param name
     * @param marks
     * @param date
     */
    public Student(String name, double [] marks, GregorianCalendar date){
        this.name = name;
        this.marks = marks;
        this.averageMark = gerAverageMark();
        this.date = date;
    }
    public Student(){}


    //getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GregorianCalendar getDate() {
        return date;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public double[] getMarks() {
        return marks;
    }

    public void setMarks(double[] marks) {
        this.marks = marks;
    }

    public double getAverageMark() {
        return averageMark;
    }

    public void setAverageMark(double averageMark) {
        this.averageMark = averageMark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name = '" + name + '\'' +
                ", marks = " + Arrays.toString(marks) +
                ", averageMark = " + averageMark +
                ", date = " + date.getTime() +
                '}';
    }

    public static Comparator<Student> getSort(){
        return  new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                int d = s1.date.compareTo(s2.date);
                if (d != 0)
                    return d;
                int n = s1.name.compareTo(s2.name);
                return n;
            }
        };
    }
    public static Comparator<Student> getSortByBirthday(){
        return  new Comparator<Student>() {
            @Override
            public int compare(Student s1, Student s2) {
                int d = Integer.compare(s1.date.get(Calendar.MONTH), s2.date.get(Calendar.MONTH));
                if (d != 0)
                    return d;
                int n = Integer.compare(s1.date.get(Calendar.DAY_OF_MONTH), s2.date.get(Calendar.DAY_OF_MONTH));
                return n;
            }
        };
    }

  /*  @Override
    public int compareTo(@NonNull Student student) {
        int mark = Double.compare(averageMark, student.averageMark);
        if (mark != 0)
            return mark;
        int name = this.name.compareTo(student.name);
            return name;
    }*/
}