package com.example.olegk.hometasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Created by Oleg K on 27.03.2017.
 */

public class KraskoJavaHW7 {

    static void findingFilesWithOrgEnd(File dir) {
        String[] list = dir.list();
        for (int i = 0, n = list.length; i < n; i++) {
            if (list[i].endsWith(".org")) {
                System.out.println(list[i]);
            }
        }
    }

    static void displayingExactFile(File file) throws FileNotFoundException {
        Scanner scan = new Scanner(file);
        while (scan.hasNextLine()){
            String s = scan.nextLine();
            System.out.println(s);
        }
    }



    public static void main(String[] args) throws FileNotFoundException {
        //displaying all file which has ".org" end
        File dir = new File("C:\\Users\\test\\Documents\\HomeTasks");
        findingFilesWithOrgEnd(dir);

        //displaying exact file
        File file;
        do {
            System.out.println("please enter name of organization");
            Scanner scan = new Scanner(System.in);
            String s = scan.next();
            file = new File(s + ".org");
        }
        while (!file.exists());
        displayingExactFile(file);
    }
}
