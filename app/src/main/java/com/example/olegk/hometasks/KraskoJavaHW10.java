package com.example.olegk.hometasks;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 * Created by Oleg K on 10.04.2017.
 */

public class KraskoJavaHW10 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = 0;
        while (n < 1)
            try {
                System.out.println("enter capacity of array");
                n = Integer.parseInt(scanner.nextLine());
                if (n < 1){
                    throw new Exception();
                }
            }
            catch (NumberFormatException ex){
                System.out.println("not a number");
            }
            catch (Exception e){
                System.out.println("number should be more than 0");
            }

        Figure figure[] = new Figure[n];

        int count = 0;
        int typeOfFigure = 0;
        String name;
        double d1 = 0;
        double d2 = 0;
        while (count < figure.length){
            while (typeOfFigure < 1 || typeOfFigure > 3)
                try {
                    System.out.println("enter type of figure, 1 - rectangle, 2 - square, 3 - circle");
                    typeOfFigure = Integer.parseInt(scanner.nextLine());
                    if (typeOfFigure < 1 || typeOfFigure > 3){
                        throw new Exception();
                    }
                }
                catch (NumberFormatException ex){
                    System.out.println("not a number");
                }
                catch (Exception e){
                    System.out.println("number should be only 1, 2 or 3");
                }
                if (typeOfFigure == 1){
                    System.out.println("enter name of figure");
                    name = scanner.nextLine();
                    while (d1 <= 0){
                        try {
                            System.out.println("enter width");
                            d1 = Double.parseDouble(scanner.nextLine());
                            if (d1 <= 0){
                                throw new Exception();
                            }
                        }
                        catch (NumberFormatException ex){
                            System.out.println("not a number");
                        }
                        catch (Exception e){
                            System.out.println("number should be more than 0");
                        }
                    }
                    while (d2 <= 0){
                        try {
                            System.out.println("enter length");
                            d2 = Double.parseDouble(scanner.nextLine());
                            if (d2 <= 0){
                                throw new Exception();
                            }
                        }
                        catch (NumberFormatException ex){
                            System.out.println("not a number");
                        }
                        catch (Exception e){
                            System.out.println("number should be more than 0");
                        }
                    }
                    figure[count] = new Rectangle(name, "Rectangle", d1, d2);
                    d1 = 0;
                    d2 = 0;
                }
                else if (typeOfFigure == 2){
                    System.out.println("enter name of figure");
                    name = scanner.nextLine();
                    while (d1 <= 0){
                        try {
                            System.out.println("enter length");
                            d1 = Double.parseDouble(scanner.nextLine());
                            if (d1 <= 0){
                                throw new Exception();
                            }
                        }
                        catch (NumberFormatException ex){
                            System.out.println("not a number");
                        }
                        catch (Exception e){
                            System.out.println("number should be more than 0");
                        }
                    }
                    figure[count] = new Square(name, "Square", d1);
                    d1 = 0;
                }
                else {
                    System.out.println("enter name of figure");
                    name = scanner.nextLine();
                    while (d1 <= 0){
                        try {
                            System.out.println("enter radius");
                            d1 = Double.parseDouble(scanner.nextLine());
                            if (d1 <= 0){
                                throw new Exception();
                            }
                        }
                        catch (NumberFormatException ex){
                            System.out.println("not a number");
                        }
                        catch (Exception e){
                            System.out.println("number should be more than 0");
                        }
                    }
                    figure[count] = new Circle(name, "Circle", d1);
                    d1 = 0;
                }
            count++;
            typeOfFigure = 0;
        }
        for (Figure f:
             figure) {
            f.display();

        }

    }

}

 abstract class Figure{
    //fields
    private String name;
    private String type;

    //methods
    abstract public double square();
    abstract public void display();



    //getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    //constructors
    public Figure(){}

    /**
     *
     * @param name
     * @param type
     */
    public Figure(String name, String type) {
        this.name = name;
        this.type = type;
    }

}

class Rectangle extends Figure{

    //fields
    private double width;
    private double length;

    //methods
    @Override
    public double square() {
        return width * length;
    }

    @Override
    public void display() {
        System.out.print("Figure \"" + getName() + "\" type of " + getType() + " has width " + width + " and length " + length);
        System.out.printf(", square is %.2f\n", square());
    }

    //getters and setters

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    //constructors
    public Rectangle(){}

    /**
     *
     * @param name
     * @param type
     * @param width
     * @param length
     */
    public Rectangle(String name, String type, double width, double length) {
        super(name, type);
        this.width = width;
        this.length = length;
    }
}

class Square extends Figure{

    //fields
    private double length;

    //methods
    @Override
    public double square() {
        return length * length;
    }

    @Override
    public void display() {
        System.out.print("Figure \"" + getName() + "\" type of " + getType() + " has length " + length);
        System.out.printf(", square is %.2f\n", square());
    }


    //getters and setters

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }


    //constructors
    public Square(){}

    /**
     *
     * @param name
     * @param type
     * @param length
     */
    public Square(String name, String type, double length) {
        super(name, type);
        this.length = length;
    }
}

class Circle extends Figure{

    //fields
    private double radius;

    //methods
    @Override
    public double square() {
        return Math.PI * Math.pow(radius, 2);
    }

    @Override
    public void display() {
        System.out.print("Figure \"" + getName() + "\" type of " + getType() + " has radius " + radius);
        System.out.printf(", square is %.2f\n", square());
    }


    //getters and setters
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


    //constructors
    public Circle(){}

    /**
     *
     * @param name
     * @param type
     * @param radius
     */
    public Circle(String name, String type, double radius) {
        super(name, type);
        this.radius = radius;
    }
}