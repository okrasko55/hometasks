package com.example.olegk.hometasks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Created by Oleg K on 18.04.2017.
 */

public class KraskoJavaHW11 {

    public static void fromInToCollection(List<Employee> list){
        Scanner scanner = new Scanner(System.in);
        int flag = 1;
        String tabelNumber = "0";
        BigDecimal salary = null;
        boolean isNumber = false;
        boolean isDouble = false;
        while (flag == 1) {
            System.out.println("enter Last Name");
            String lastName = scanner.nextLine();
            System.out.println("enter First Name");
            String firstName = scanner.nextLine();
            System.out.println("enter position");
            String position = scanner.nextLine();
            System.out.println("enter gender(Male/Female)");
            String gender = scanner.nextLine();
            while (tabelNumber.length() != 4 || isNumber == false){
                isNumber = false;
                System.out.println("enter number(4 digits)");
                tabelNumber = scanner.nextLine();
                try {
                    int i = Integer.parseInt(tabelNumber);
                    isNumber = true;
                }
                catch (NumberFormatException ex){
                    System.out.println("not a number");
                }
            }
            System.out.println("enter salary");
            while (isDouble == false)
                try {
                    double d = Double.parseDouble(scanner.nextLine());
                    isDouble = true;
                    salary = new BigDecimal(d);
                    salary = salary.setScale(2, BigDecimal.ROUND_CEILING);
                }
                catch (NumberFormatException ex){
                    System.out.println("not a number");
                }
            list.add(new Employee(firstName, lastName, position, gender, tabelNumber, salary));
            System.out.println("more? if yes - enter 1, not - enter 0");
            try{
                flag = Integer.parseInt(scanner.nextLine());
            }
            catch (NumberFormatException ex){
                System.out.println(ex.getMessage());
            }
            tabelNumber = "0";
            isDouble = false;
            isNumber = false;
        }
    }

    public static void fromCollectionToFile(List<Employee> list, File file){
        try (FileWriter fw = new FileWriter(file);){
            for (Employee e:
                 list) {
                fw.write(e.getFirstName());
                fw.write(", ");
                fw.write(e.getLastName());
                fw.write(", ");
                fw.write(e.getPosition());
                fw.write(", ");
                fw.write(e.getGender());
                fw.write(", ");
                fw.write(e.getTabelNumber());
                fw.write(", ");
                fw.write(e.getSalary() + "");
                fw.write("\n");
            }
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter name of file");
        String name = scanner.nextLine();
        File file = new File(name + ".org");
        try {
            if (file.exists()){
                System.out.println("File with the same name exists, if you want to create new file - enter 1 and write new name for file, if you want rewrite the file enter 0");
                int ifExists = Integer.parseInt(scanner.nextLine());
                if (ifExists == 1){
                    File file1 = new File(scanner.nextLine() + ".org");
                    file = file1;
                }
            }
        }
        catch (NumberFormatException ex){
            System.out.println(ex.getMessage());
        }

        List<Employee> list = new ArrayList<>();
        fromInToCollection(list);
        fromCollectionToFile(list,file);
    }
}

class Employee{
    //variables
    private String firstName;
    private String lastName;
    private String position;
    private String gender;
    private String tabelNumber;
    private BigDecimal salary;


    //getters and setters
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTabelNumber() {
        return tabelNumber;
    }

    public void setTabelNumber(String tabelNumber) {
        this.tabelNumber = tabelNumber;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    //constructors
    /**
     *
     * @param firstName
     * @param lastName
     * @param position
     * @param gender
     * @param tabelNumber
     * @param salary
     */
    public Employee(String firstName, String lastName, String position, String gender, String tabelNumber, BigDecimal salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.gender = gender;
        this.tabelNumber = tabelNumber;
        this.salary = salary;
    }

}