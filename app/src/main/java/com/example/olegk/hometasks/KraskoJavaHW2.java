package com.example.olegk.hometasks;

import java.util.Random;

/**
 * Created by Oleg K on 03.03.2017.
 */

public class KraskoJavaHW2 {
    public static void main(String[] args) {
        //declaration of variables
        Random rand = new Random();
        int minArr = 10;
        int maxArr = 20;
        int minB = 43;
        int maxB = 53;
        int minL = 21;
        int maxL = 41;
        int N = minArr + rand.nextInt(maxArr - minArr + 1);
        double [][] coordinates = new double[2][N];
        int mostWestIndex = 0;
        int mostNorthIndex = 0;

        //initializing array
        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                for (int j = 0; j < N; j++) {
                    coordinates[i][j] = minB + rand.nextInt(maxB - minB + 1);
                    coordinates[i][j] += Math.random();
                    if(coordinates[i][j] > 53.00)
                        coordinates[i][j] = 53.00;
                }
            } else {
                for (int j = 0; j < N; j++) {
                    coordinates[i][j] = minL + rand.nextInt(maxL - minL + 1);
                    coordinates[i][j] += Math.random();
                    if(coordinates[i][j] > 41.00)
                        coordinates[i][j] = 41.00;
                }
            }
        }
        //finding the most west and the most north coordinates
        double mostNorth = coordinates[0][0];
        double mostWest = coordinates[1][0];
        for(int i = 0; i < 2; i++){
            if(i == 0){
                for(int j = 0; j < N; j++) {
                    if (mostNorth < coordinates[i][j]){
                        mostNorth = coordinates[i][j];
                        mostNorthIndex = j;
                    }
                }

            }else{
                for(int j = 0; j < N; j++) {
                    if (mostWest > coordinates[i][j]) {
                        mostWest = coordinates[i][j];
                        mostWestIndex = j;
                    }
                }
            }
        }
        //showing results
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < N; j++){
                System.out.printf("%.2f", coordinates[i][j]);
                System.out.print("\t");
            }
            System.out.println();
        }
        System.out.print("the most west point is ");
        System.out.printf("%.2f", coordinates[0][mostWestIndex]);
        System.out.print(" and ");
        System.out.printf("%.2f", coordinates[1][mostWestIndex]);
        System.out.println();
        System.out.print("the most north point is ");
        System.out.printf("%.2f", coordinates[0][mostNorthIndex]);
        System.out.print(" and ");
        System.out.printf("%.2f", coordinates[1][mostNorthIndex]);
    }
}
