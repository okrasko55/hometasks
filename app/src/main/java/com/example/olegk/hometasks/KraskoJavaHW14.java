package com.example.olegk.hometasks;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;

import java.util.Random;

import edu.princeton.cs.introcs.StdRandom;

/**
 * Created by Oleg K on 15.05.2017.
 */

public class KraskoJavaHW14 {
    public static void main(String[] args) throws InterruptedException {

        int n = (int)Math.pow(10, 5);
        Random r = new Random();
        double arr [] = new double [n];
        for (int i = 0, N = arr.length; i < N; i++) {
            arr[i] = r.nextInt(101);
            arr[i] += Math.random();
        }
        int countOfCores = Runtime.getRuntime().availableProcessors();
        int m = n / countOfCores + 1;
        long beginMulti = System.currentTimeMillis();
        MultiThreads [] multiArr = new MultiThreads[countOfCores];
        for (int k = 0; k < countOfCores; k++){
            multiArr[k] = new MultiThreads(k * m, (k + 1) * m < n ? (k + 1) * m : n, arr);
            multiArr[k].start();
        }
        for (MultiThreads multi:
             multiArr) {
            multi.join();
        }
        double sum = 0;
        for (MultiThreads multi:
             multiArr) {
            sum += multi.getSum();
        }
        sum /= arr.length;
        System.out.printf("Average calculating by threads is %.2f\n", sum);
        System.out.println("Time of calculating using several cores (in seconds) - " + (System.currentTimeMillis() - beginMulti) / 1000.00);



        long begin = System.currentTimeMillis();
        for (double d:
             arr) {
            sum += d;
        }
        sum /= arr.length;
        System.out.printf("Average calculating by linear way is %.2f\n", sum);
        System.out.println("Time of calculating using one core (in seconds) - " + (System.currentTimeMillis() - begin) / 1000.00);
    }

}
class MultiThreads extends Thread{
    //variables
    private int start;
    private int finish;
    private double sum;
    private double [] arr;


    //getters and setters
    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getFinish() {
        return finish;
    }

    public void setFinish(int finish) {
        this.finish = finish;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public double[] getArr() {
        return arr;
    }

    public void setArr(double[] arr) {
        this.arr = arr;
    }

    //constructors
    MultiThreads(int start, int finish, double [] arr){
        this.start = start;
        this.finish = finish;
        this.arr = arr;
    }

    @Override
    public void run() {
        for (int i = start; i < finish; i++){
            sum += arr[i];
        }
        System.out.println("The sum of " + Thread.currentThread().getName() + " : " + sum);
    }
}