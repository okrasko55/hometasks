package com.example.olegk.hometasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.WeakHashMap;

import edu.princeton.cs.introcs.In;
import edu.princeton.cs.introcs.StdOut;

/**
 * Created by Oleg K on 28.04.2017.
 */

public class Lesson27_04 {
    public static void fromFileToCollection(File file, Set<Holiday> set, Map<String, String> map) {
        try (Scanner scanner = new Scanner(file);) {
            int year = 0;
            int month = 0;
            int day = 0;
            while (scanner.hasNextLine()){
                String s = scanner.nextLine();
                String[] arr = s.split(" ");
                String name = "";
                for (int i = 1, n = arr.length; i < n; i++){
                    if (i == 1){
                        name += arr[i];
                    }else {
                        name += " " + arr[i];
                    }

                }
                String[] dateArr = arr[0].split("/");
                try{
                    year = Integer.parseInt(dateArr[0]);
                    month = Integer.parseInt(dateArr[1]);
                    day = Integer.parseInt(dateArr[2]);
                }
                catch (NumberFormatException e){
                    System.out.println(e.getMessage());
                }
                arr[arr.length - 1] = arr[arr.length - 1].substring(1,arr[arr.length - 1].length() - 1);
                map.put(arr[arr.length - 1], arr[arr.length - 1]);
                set.add(new Holiday(name, new GregorianCalendar(year, month - 1, day), map.get(arr[arr.length - 1])));
            }

        }
        catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }
    }

    public static void calendarOfNearestHolidays(Set<Holiday> set){
        Calendar currentDate = Calendar.getInstance();
        Calendar tomorrowDate = Calendar.getInstance();
        tomorrowDate.roll(Calendar.DAY_OF_MONTH, true);
        Calendar [] nextDaysArr = {Calendar.getInstance(),
                                Calendar.getInstance(),
                                Calendar.getInstance(),
                                Calendar.getInstance(),
                                Calendar.getInstance()};
        int d = 2;
        for (int i = 0, n = nextDaysArr.length; i < n; i++){
            nextDaysArr[i].add(Calendar.DAY_OF_MONTH, d);
            d++;
        }
   /*     System.out.println("===========================");
        for (Calendar c:
             nextDaysArr) {
            System.out.println(c.getTime());
        }
        System.out.println("===========================");*/

        List<Holiday> today = new LinkedList<>();
        List<Holiday> tomorrow = new LinkedList<>();
        List<Holiday> [] nextDays = new List[5];
        for (int i = 0, n = nextDays.length; i < n; i++){
            nextDays[i] = new LinkedList<>();
        }
        for (Holiday h:
             set) {
            if (h.getDate().get(Calendar.MONTH) == currentDate.get(Calendar.MONTH) &&
                h.getDate().get(Calendar.DAY_OF_MONTH) == currentDate.get(Calendar.DAY_OF_MONTH)){
                    today.add(h);
            }
            if (h.getDate().get(Calendar.MONTH) == tomorrowDate.get(Calendar.MONTH) &&
                h.getDate().get(Calendar.DAY_OF_MONTH) == tomorrowDate.get(Calendar.DAY_OF_MONTH)){
                    tomorrow.add(h);
            }
            if (h.getDate().get(Calendar.MONTH) == nextDaysArr[0].get(Calendar.MONTH) &&
                h.getDate().get(Calendar.DAY_OF_MONTH) == nextDaysArr[0].get(Calendar.DAY_OF_MONTH)){
                nextDays[0].add(h);
            }
            if (h.getDate().get(Calendar.MONTH) == nextDaysArr[1].get(Calendar.MONTH) &&
                h.getDate().get(Calendar.DAY_OF_MONTH) == nextDaysArr[1].get(Calendar.DAY_OF_MONTH)){
                nextDays[1].add(h);
            }
            if (h.getDate().get(Calendar.MONTH) == nextDaysArr[2].get(Calendar.MONTH) &&
                h.getDate().get(Calendar.DAY_OF_MONTH) == nextDaysArr[2].get(Calendar.DAY_OF_MONTH)) {
                nextDays[2].add(h);
            }
            if (h.getDate().get(Calendar.MONTH) == nextDaysArr[3].get(Calendar.MONTH) &&
                h.getDate().get(Calendar.DAY_OF_MONTH) == nextDaysArr[3].get(Calendar.DAY_OF_MONTH)) {
                nextDays[3].add(h);
            }
            if (h.getDate().get(Calendar.MONTH) == nextDaysArr[4].get(Calendar.MONTH) &&
                h.getDate().get(Calendar.DAY_OF_MONTH) == nextDaysArr[4].get(Calendar.DAY_OF_MONTH)){
                nextDays[4].add(h);
            }
        }
        System.out.println("Today: ");
        for (Holiday h:
             today) {
            System.out.println(h.getName());
        }
        System.out.println("====================================");
        System.out.println("Tomorrow: ");
        for (Holiday h:
             tomorrow) {
            System.out.println(h.getName());
        }
        System.out.println("====================================");
        System.out.println("Soon: ");
        for (int i = 0, n = nextDays.length; i < n; i++){
            if (!nextDays[i].isEmpty()){
                String [] s = nextDays[i].get(0).getDate().getTime().toString().split(" ");
                System.out.print(s[0] + ", " + s[2] + " of " + s[1]);
                int q = 0;
                for (Holiday h:
                        nextDays[i]) {
                    if (q == 0) {
                        System.out.println(" - " + h.getName());
                    }
                    else {
                        System.out.println("\t\t\t - " + h.getName());
                    }
                    q++;
                }
            }

        }


    }

    public static void main(String[] args) {
        Set<Holiday> set = new TreeSet<>();
        Map<String, String> map = new WeakHashMap<>();
        File file = new File("holidays.txt");
        fromFileToCollection(file, set, map);
        calendarOfNearestHolidays(set);

    }

}
class Holiday implements Comparable<Holiday>{

    //variables
    private GregorianCalendar date;
    private String name;
    private String country;



    //Getters & Setters
    public GregorianCalendar getDate() {
        return date;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    //constructors
    public Holiday(String name, GregorianCalendar date, String country){
        this.name = name;
        this.date = date;
        this.country = country;


    }
    public Holiday(){}



    @Override
    public int compareTo(Holiday o) {
        int date = this.date.compareTo(o.date);
        if (date != 0)
            return date;
        int name = this.name.compareTo(o.name);
        return name;
    }
    @Override
    public String toString() {
        return "Holiday{" +
                "date = " + date.getTime() +
                ", name = '" + name + '\'' +
                '}';
    }
}