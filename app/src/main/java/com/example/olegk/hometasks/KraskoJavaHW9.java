package com.example.olegk.hometasks;

import java.util.Scanner;

/**
 * Created by Oleg K on 03.04.2017.
 */
class Rectangle1{
    private double length;
    private double width;

    public double square(){
        return length * width;
    }

    public void showSquare(){
        System.out.println(square());
    }

    public double perimeter(){
       return  (length + width) *2;
    }

    public void showPerimeter(){
        System.out.println(perimeter());
    }


    public Rectangle1(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public Rectangle1(Scanner scanner) {
        System.out.println("enter lenght, please:");
        length = scanner.nextDouble();
        System.out.println("enter width, please:");
        width = scanner.nextDouble();
    }

    public Rectangle1() {
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
public class KraskoJavaHW9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Rectangle1 rectangle = new Rectangle1(scanner);
        rectangle.showPerimeter();
        rectangle.showSquare();
    }
}
