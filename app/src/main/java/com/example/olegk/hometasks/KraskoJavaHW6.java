package com.example.olegk.hometasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import edu.princeton.cs.introcs.StdRandom;

/**
 * Created by Oleg K on 20.03.2017.
 */

public class KraskoJavaHW6 {
    //generating array
    public static double[][] randArr(int N, int M, double min, double max) {
        double mas[][] = new double[N][M];
        for (int i = 0, n = mas.length; i < n; i++) {
            for (int j = 0, l = mas[i].length; j < l; j++) {
                mas[i][j] = StdRandom.uniform(min, max);
            }
        }
        return mas;
    }

    //displaying array
    public static void displayArr2(double[][] arr) {
        for (int i = 0, n = arr.length; i < n; i++) {
            for (int j = 0, n1 = arr[i].length; j < n1; j++) {
                System.out.print(i + " " + j + ": ");
                System.out.printf("%.4f", arr[i][j]);
                System.out.print("\t");
            }
            System.out.println();
        }
    }

    //insertion an array to the file
    public static void saveArrToFile(double arr[][], File file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(arr.length + " " + arr[0].length + "\n");
        for (int i = 0, n = arr.length; i < n; i++) {
            for (int j = 0, n1 = arr[i].length; j < n1; j++) {
                fileWriter.write(arr[i][j] + " ");
            }
            fileWriter.write("\n");
        }
        fileWriter.close();
    }

    //reading array from the file
    public static double [][] readFromFileArray(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileReader(file));
        int N = Integer.parseInt(scanner.next());
        int M = Integer.parseInt(scanner.next());
        double arr[][] = new double[N][M];
        for(int i = 0, n = arr.length; i < n; i++){
            for(int j = 0, n1 = arr[i].length; j < n1; j++){
                arr[i][j] = Double.parseDouble(scanner.next());
            }
        }
        scanner.close();
        return arr;
    }


    public static void main(String[] args) throws IOException {

        //receiving sizes of array from keyboard
        int N = 0;
        int M = 0;
        Scanner scanner = new Scanner(System.in);
        while (N < 1) {
            try {
                System.out.println("please enter qty of rows");
                N = Integer.parseInt(scanner.nextLine());
                if (N < 1) {
                    throw new Exception("number should be only 1 or more");
                }
            } catch (NumberFormatException ex) {
                System.out.println("please enter number, not other symbols");
            } catch (Exception e) {
                System.out.println("please enter 1 or more");
            }
        }
        while (M < 1) {
            try {
                System.out.println("please enter qty of columns");
                M = Integer.parseInt(scanner.nextLine());
                if (M < 1) {
                    throw new Exception("number should be only 1 or more");
                }
            } catch (NumberFormatException ex) {
                System.out.println("please enter number, not other symbols");
            } catch (Exception e) {
                System.out.println("please enter 1 or more");
            }
        }

        //executing tasks one by one in accordance to description
        double arr[][] = randArr(N, M, -100, 100);
        displayArr2(arr);
        File file = new File("input.txt");
        saveArrToFile(arr, file);
        double arrCopy [][] = readFromFileArray(file);
    }
}
