package com.example.olegk.hometasks;

import java.util.Scanner;

/**
 * Created by Oleg K on 07.03.2017.
 */

public class KraskoJavaHW3_additional {
    public static void main(String[] args) {
        //naming of the number
        Scanner scann = new Scanner(System.in);
        System.out.println("Please name any number from 1 to 100:");
        int questNum = scann.nextInt();

        //trying to guess by computer
        int count = 0;
        int answer = 50;
        int min = 1;
        int max = 100;
        while(answer != questNum){
            System.out.println("Computer things that number is " + answer + "\nplease help computer and give the hint: if quested number is more - type 0, but if it is less - type 1");
            int hint = scann.nextInt();
            if(hint == 1){
                max = answer;
                count++;
            }else{
                min = answer;
                count++;
            }
            answer = (min + max) / 2;
        }
        System.out.println("Computer could guess for " + count + " attempts");

    }
}
