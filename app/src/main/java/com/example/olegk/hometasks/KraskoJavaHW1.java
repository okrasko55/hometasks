package com.example.olegk.hometasks;


import java.util.Scanner;

/**
 * Created by Oleg K on 27.02.2017.
 */

public class KraskoJavaHW1 {




    public static void main(String[] args) {
        //declaration of constants and variables
        final double USD_SALE = 27.1;
        final double USD_BUY = 26.8;
        final double EUR_SALE = 28.8;
        final double EUR_BUY = 28.2;
        int inquiryType = 3;
        int amountHryvnias = -1;
        int amountCurrency = -1;
        //String currencyType = "";
        int currencyType = 3;
        int con = 1;
        Scanner scanner = new Scanner(System.in);

        //receiving input data
        while (con == 1) {
            while (inquiryType < 1 || inquiryType > 2) {
                try {
                    System.out.println("do you want to buy currency or sale? in BUY case - enter 1, in SALE case - enter 2:");
                    inquiryType = Integer.parseInt(scanner.nextLine());
                    if (inquiryType < 1 || inquiryType > 2) {
                        throw new Exception("number should be only 1 or 2");
                    }
                } catch (NumberFormatException ex) {
                    System.out.println("please enter number, not other symbols");
                } catch (Exception e) {
                    System.out.println("please enter 1 or 2, not other number");
                }
            }
            if (inquiryType == 1) {
                while (amountHryvnias % 50 != 0 || amountHryvnias < 1) {
                    try {
                        System.out.println("please enter an amount multiple of 50");
                        amountHryvnias = Integer.parseInt(scanner.nextLine());
                        if (amountHryvnias % 50 != 0 || amountHryvnias < 1) {
                            throw new Exception();
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("please enter just number, not other symbols");
                    } catch (Exception e) {
                        System.out.println("it is not of multiply of 50 or bellow zero, please enter correct amount");
                    }
                }
            } else {
                while (amountCurrency % 10 != 0 || amountCurrency < 1) {
                    try {
                        System.out.println("please enter an amount multiple of 10");
                        amountCurrency = Integer.parseInt(scanner.nextLine());
                        if (amountCurrency % 10 != 0 || amountCurrency < 1) {
                            throw new Exception();
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("please enter just number, not other symbols");
                    } catch (Exception e) {
                        System.out.println("it is not of multiply of 10 or bellow zero, please enter correct amount");
                    }
                }
            }
        /*while(!currencyType.equals("USD") || !currencyType.equals("usd") || !currencyType.equals("EUR") || !currencyType.equals("eur")){
            try{
                System.out.println("what type of currency, in USD case - enter \"USD\", in EUR case - enter \"EUR\"");
                currencyType = scanner.nextLine();
                if(!currencyType.equals("USD") || !currencyType.equals("usd") || !currencyType.equals("EUR") || !currencyType.equals("eur")){
                    throw new Exception("wrong string");
                }
            }catch (Exception ex){
                System.out.println("please enter correct data");
            }
        }*/
            while (currencyType < 1 || currencyType > 2) {
                try {
                    System.out.println("what type of currency, in USD case - enter 1, in EUR case - enter 2");
                    currencyType = Integer.parseInt(scanner.nextLine());
                    if (currencyType < 1 || currencyType > 2) {
                        throw new Exception();
                    }
                } catch (NumberFormatException ex) {
                    System.out.println("please enter number, not other symbols");
                } catch (Exception e) {
                    System.out.println("please enter 1 or 2, not other number");
                }
            }

            //calculating
            double d = amountHryvnias / USD_SALE / 10;
            int amountUSD = (int) d * 10;
            double e = amountHryvnias / EUR_SALE / 10;
            int amountEUR = (int) e * 10;


            //showing results
            if (inquiryType == 2) {
                if (currencyType == 1)
                    System.out.println("you will receive: ₴" + (double) amountCurrency * USD_BUY);
                else
                    System.out.println("you will receive: ₴" + (double) amountCurrency * EUR_BUY);
            } else {
                if (currencyType == 1)
                    System.out.println("you can get $" + (double) amountUSD +
                            " for yours ₴" + (double) amountHryvnias +
                            " and you should pay for this ₴" + (double) amountUSD * USD_SALE);
                else
                    System.out.println("you can get €" + (double) amountEUR +
                            " for yours ₴" + (double) amountHryvnias +
                            " and you should pay for this ₴" + (double) amountEUR * EUR_SALE);
            }
            inquiryType = 3;
            amountHryvnias = -1;
            amountCurrency = -1;
            try {
                System.out.println("do you want to repeat the procedure, if yes enter 1, no - enter 2 or any other number");
                con = Integer.parseInt(scanner.nextLine());
            }catch (NumberFormatException ex){
                System.out.println("please enter number, not other symbols");
            }
        }
    }

}
