package com.example.olegk.hometasks;

import java.util.Collections;

/**
 * Created by Oleg K on 15.05.2017.
 */

public class Lesson14_05 {
    public static void main(String[] args) throws InterruptedException {
//		CommonResource commonResource = new CommonResource();
//
//		for (int i = 1; i < 6; i++){
//			TestLesson14_05 test = new TestLesson14_05(commonResource);
//			test.start();
//		}
        CommonResource commonResource = new CommonResource();
        FromAToB aToB = new FromAToB(commonResource);
        FromBToA bToA = new FromBToA(commonResource);
        aToB.start();
        bToA.start();


    }

}

class TestLesson14_05 extends Thread{


    CommonResource res;
    TestLesson14_05(CommonResource res){
        this.res = res;
    }

    @Override
    public void run() {
        synchronized (res) {
            res.x = 1;
            for (int i = 1; i < 5; i++){
                System.out.printf("%s %d \n", Thread.currentThread().getName(), res.x);
                res.x++;
                try{
                    Thread.sleep(100);
                }
                catch(InterruptedException e){}
            }
        }

    }

}

class CommonResource{

    int x = 0;
}

class FromAToB extends Thread {

    CommonResource commonResource;

    //constructors
    /**
     *
     * @param commonResource
     */
    FromAToB(CommonResource commonResource){
        this.commonResource = commonResource;
    }

    @Override
    public void run() {
        synchronized (commonResource) {
            System.out.println(Thread.currentThread().getName() + " Start!");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
            System.out.println(Thread.currentThread().getName() + " Finish!");
        }
    }

}

class FromBToA extends Thread {

    CommonResource commonResource;

    //constructors
    /**
     *
     * @param commonResource
     */
    FromBToA(CommonResource commonResource){
        this.commonResource = commonResource;
    }



    @Override
    public void run() {
        synchronized (commonResource) {
            System.out.println(Thread.currentThread().getName() + " Start!");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
            System.out.println(Thread.currentThread().getName() + " Finish!");
        }
    }

}
