package com.example.olegk.hometasks;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by Oleg K on 06.03.2017.
 */

public class KraskoJavaHW3 {

    public static void main(String[] args) {
        //THE GAME WHERE THE PLAYER IS GUESSING

        //receiving input data
        Random rnd = new Random();
        int questNum = rnd.nextInt(100) + 1;
        int ansNum;

        //trying to guess
        Scanner scann = new Scanner(System.in);
        do{
            System.out.println("please try to guess number from 1 to 100, enter your variant:");
            ansNum = scann.nextInt();
            if(ansNum == questNum)
                System.out.println("Congratulations! you guessed!");
            else if(ansNum > questNum)
                System.out.println("please try again, unknown number is less");
            else
                System.out.println("please try again, unknown number is more");
        }while(ansNum != questNum);

        //THE GAME WHERE THE COMPUTER IS GUESSING

        /*//naming of the number
        Scanner scann = new Scanner(System.in);
        System.out.println("Please name any number:");
        int questNum = scann.nextInt();

        //trying to guess by computer
        int count = 0;
        int answer = 50;
        int min = 1;
        int max = 100;
        while(answer != questNum){
            System.out.println("Computer things that number is " + answer + "\nplease help computer and give the hint: if quested number is more - type 0, but if it is less - type 1");
            int hint = scann.nextInt();
            if(hint == 1){
                max = answer;
                count++;
            }else{
                min = answer;
                count++;
            }
            answer = (min + max) / 2;
        }
        System.out.println("Computer could guess for " + count + " attempts");*/

    }
}
